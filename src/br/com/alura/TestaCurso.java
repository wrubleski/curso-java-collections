package br.com.alura;

public class TestaCurso {
	public static void main(String[] args) {
		Curso javaColecoes = new Curso("Dominando as colecoes do java", "Pauolo Silvera");

//		javaColecoes.getAulas().add(new Aula("Trabalhando com arraylist", 21));
		javaColecoes.adiciona(new Aula("Trabalhando com arraylist", 21));
		javaColecoes.adiciona(new Aula("Criando uma Aula", 20));
		javaColecoes.adiciona(new Aula("Modelando com colecoes", 24));

		System.out.println(javaColecoes.getAulas());

		System.out.println(javaColecoes);
	}
}
