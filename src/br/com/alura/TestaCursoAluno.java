package br.com.alura;

import java.util.Iterator;
import java.util.Set;

public class TestaCursoAluno {
	public static void main(String[] args) {
		Curso javaColecoes = new Curso("Dominando as colecoes do java", "Pauolo Silvera");
		javaColecoes.adiciona(new Aula("Trabalhando com arraylist", 21));
		javaColecoes.adiciona(new Aula("Criando uma Aula", 20));
		javaColecoes.adiciona(new Aula("Modelando com colecoes", 24));

		Aluno a1 = new Aluno("Rodrigo", 123456);
		Aluno a2 = new Aluno("Guilherme", 123456);
		Aluno a3 = new Aluno("Mauricio", 123456);

		javaColecoes.matricula(a1);
		javaColecoes.matricula(a2);
		javaColecoes.matricula(a3);

		System.out.println("Matriculados: ");
		javaColecoes.getAlunos().forEach(a -> System.out.println(a));

		Aluno rodrigo = new Aluno("Rodrigo", 123456);
		System.out.println(javaColecoes.estaMatriculado(rodrigo));
		System.out.println(a1.equals(rodrigo));

		System.out.println("---------------------------\n\n");

		Set<Aluno> alunos = javaColecoes.getAlunos();
		Iterator<Aluno> iterador = alunos.iterator();

		while (iterador.hasNext()) {
			Aluno proximo = iterador.next();
			System.out.println(proximo);
		}

	}
}
