package br.com.alura;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
//import java.util.Set;

public class TestaAlunos {

	public static void main(String[] args) {

		Collection<String> alunos = new HashSet<>();
		alunos.add("Rodrigo Turini");
		alunos.add("Alberto Souza");
		alunos.add("Nico Steppat");
		alunos.add("Nico Steppat");

		System.out.println(alunos.contains("Rodrigo Turini"));
		alunos.remove("Nico Steppat");
		// alunos.get(3); // set nao tem indice
		System.out.println(alunos.size());
		System.out.println(alunos);

		List<String> listaAlunos = new ArrayList<>(alunos);
		System.out.println(listaAlunos);

		Collection<Integer> numeros = new ArrayList<Integer>();
//		Collection<Integer> numeros = new HashSet<>();

		long inicio = System.currentTimeMillis();

		for (int i = 1; i <= 50000; i++) {
			numeros.add(i);
		}

		for (Integer numero : numeros) {
			numeros.contains(numero);
		}

		long fim = System.currentTimeMillis();

		long tempoDeExecucao = fim - inicio;

		System.out.println("Tempo gasto: " + tempoDeExecucao);

	}
}