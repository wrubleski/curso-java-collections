package br.com.alura;

public class TestaBuscaAlunos {
	public static void main(String[] args) {
		Curso javaColecoes = new Curso("Dominando as colecoes do java", "Pauolo Silvera");
		javaColecoes.adiciona(new Aula("Trabalhando com arraylist", 21));
		javaColecoes.adiciona(new Aula("Criando uma Aula", 20));
		javaColecoes.adiciona(new Aula("Modelando com colecoes", 24));

		Aluno a1 = new Aluno("Rodrigo", 123456);
		Aluno a2 = new Aluno("Guilherme", 512343);
		Aluno a3 = new Aluno("Mauricio", 4231);

		javaColecoes.matricula(a1);
		javaColecoes.matricula(a2);
		javaColecoes.matricula(a3);

		Aluno aluno = javaColecoes.buscaMatricula(123456);

		System.out.println(aluno);

	}
}
